<?php
session_start();
include 'Connection.php';
if(isset($_REQUEST['Admin'])){
    header('location: index.php');
}    
?>
<!DOCTYPE html>
<html>
<head>
	<title>Add New User</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<style>
    .bs-example{
        margin: 20px;        
    }
</style>
</head>
<body>
	<div class="container">
		<div class="row" >
			<form class="form-inline">
				<div class="form-group mr-2">
				    <label class="sr-only" for="inputEmail">Email</label>
				     <input type="email" class="form-control" id="inputEmail" placeholder="Email">
				 </div>
				    <div class="form-group mr-2">
				        <label class="sr-only" for="inputPassword">Password</label>
				        <input type="password" class="form-control" id="inputPassword" placeholder="Password">
				    </div>
				    <div class="form-group mr-2">        
				        <label><input type="checkbox" class="mr-1"> Remember me</label>
				    </div>
				    <button type="submit" class="btn btn-primary">Sign in</button>
			</form>
		</div>
	</div>
</body>
</html>