
<?php

//rms.php

class rms
{
	public $base_url = 'http://localhost/source-code-restaurant-management-system-version-1/';
	public $connect;
	public $query;
	public $statement;
	public $cur;

	function rms()
	{
		$this->connect = new PDO("mysql:host=localhost;dbname=rms", "root", "");
		if($this->Set_timezone() != '')
		{
			date_default_timezone_set($this->Set_timezone());
		}

		$temp_cur = $this->Get_currency_symbol();
		if($temp_cur != '')
		{
			$this->cur = $temp_cur;
		}
		session_start();
	}
}
?>